# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SampleApp', '0009_auto_20151107_1002'),
    ]

    operations = [
        migrations.RenameField(
            model_name='unifamily',
            old_name='familylastname',
            new_name='familyname',
        ),
    ]
