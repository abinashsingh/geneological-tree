# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('SampleApp', '0016_auto_20151107_1927'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='familylastname',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='uniquename',
            field=models.CharField(default=datetime.datetime(2015, 11, 8, 4, 24, 50, 167000, tzinfo=utc), unique=True, max_length=100),
            preserve_default=False,
        ),
    ]
