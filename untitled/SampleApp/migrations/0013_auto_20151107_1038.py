# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SampleApp', '0012_auto_20151107_1018'),
    ]

    operations = [
        migrations.RenameField(
            model_name='unifamily',
            old_name='famname',
            new_name='famm',
        ),
    ]
