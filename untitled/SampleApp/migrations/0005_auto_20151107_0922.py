# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SampleApp', '0004_auto_20151107_0916'),
    ]

    operations = [
        migrations.RenameField(
            model_name='unifamily',
            old_name='uniquename',
            new_name='uniname',
        ),
    ]
