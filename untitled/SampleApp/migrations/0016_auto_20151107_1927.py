# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SampleApp', '0015_auto_20151107_1904'),
    ]

    operations = [
        migrations.RenameField(
            model_name='person',
            old_name='membername',
            new_name='memberfirstname',
        ),
        migrations.AddField(
            model_name='person',
            name='memberlastname',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='person',
            name='membermiddlename',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
