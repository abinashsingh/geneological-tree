# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SampleApp', '0002_auto_20151105_2215'),
    ]

    operations = [
        migrations.CreateModel(
            name='Unifamily',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('familylastname', models.CharField(max_length=100)),
                ('uniquename', models.CharField(max_length=50)),
                ('username', models.CharField(max_length=30)),
            ],
        ),
    ]
