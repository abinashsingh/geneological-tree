"""untitled URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
    urlpatterns = [
    #url(r'^admin/', include(admin.site.urls)),
    #url(r'^hello/', 'SampleApp.views.hello_template')

    #url(r'^$', 'SampleApp.views.hello_template'),
    url(r'^login/$', 'django.contrib.auth.views.login'),
    url(r'^$', 'SampleApp.views.main_page'),
    url(r'^register/$', register_page),
    url(r'^addmember/', 'SampleApp.views.form_template'),
    #url(r'^login/', 'SampleApp.views.home'),
    url(r'^senddata/', 'SampleApp.views.send_template', name='person')
]
"""
from django.conf import settings
from django.conf.urls import include, url, patterns
from django.conf.urls.static import static
from django.contrib import admin
from SampleApp.views import register_page, main_page, logout_page, Tree_Main, Search_Main, Delete_Data

urlpatterns = [
url(r'^$', 'django.contrib.auth.views.login'),
url(r'^main/$', main_page),
#(r'^user/(\w+)/$', user_page),
url(r'^register/$', register_page),
 url(r'^home/', 'SampleApp.views.form_template'),
 url(r'^addFamilyName/', 'SampleApp.views.family_template'),
 url(r'^sirname/$', 'SampleApp.views.fam_template'),
 url(r'^addmem/$', 'SampleApp.views.mem_template'),
 url(r'^tree/$', 'SampleApp.views.tree_page'),
 url(r'^search/$', 'SampleApp.views.search_page'),
 url(r'^treedetail/(?P<relation>[\w|\W]+)$', Tree_Main, name="tree_location"),
 url(r'^deletemember/(?P<relation>[\w|\W]+)/(?P<id>[\w|\W]+)$', Delete_Data, name="delete_location"),
 url(r'^searchdetail/(?P<relation>[\w|\W]+)/(?P<uniquename>[\w|\W]+)$', Search_Main, name="search_location"),
#(r'^login/$', 'django.contrib.auth.views.login'),
 url(r'^logout/$', logout_page)

#(r'^logout/$', logout_page),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)